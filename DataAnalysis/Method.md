## Pași pentru manipularea și analiza datelor în Excel și InfluxDB

### Pasul 0: Analiza grafica a seturilor de date pentru a determina un interval de timp in care seturile de date sunt consistente - pentru seria SENSE1-15 si dustrak.

### Pasul 1: Interogarea bazei de date InfluxDB
Extragerea datelor agregate orar pentru fiecare set de date SENSE, cu o precizie de două zecimale:

```sql
SELECT ROUND(mean("value") * 100) / 100 FROM /^mqtt.lora.ttn.beia-libelium.SENSE([1-9]|1[0-5]).PM10/ WHERE time >= 1648630177377ms and time <= 1650458606865ms GROUP BY time(1h) fill(none);
```
### Pasul 2: Conversia timbrului Unix în timbru Excel
Se împarte timbrul Unix la 86400000 (numărul de milisecunde într-o zi) și se adaugă epoca Excel (25569).

### Pasul 3: Procesarea titlurilor în Excel
Se folosesc funcțiile MID() și SEARCH() pentru a extrage numărul SENSE dintr-un șir de text.

### Pasul 4: Compararea coloanelor de timp în două foi și detectarea valorilor lipsă
Se folosesc funcțiile IF() și MATCH() pentru a compara coloanele de timp din ambele foi și pentru a detecta valorile lipsă.

### Pasul 5: Prima analiza
Într-o primă analiză, s-au generat statisticile descriptive pentru seturile de date:
SENSE 1, 2, 3, 4 și dustrak (fiind eliminate coloanele care conțineau "undefined" / lipsa valori pentru intervalul de timp 28.03-29.03).

S-au înlocuit valorile "undefined" din seturile de date cu media coloanei folosind formula:
`=IFERROR(IF(B2="undefined", ROUND(AVERAGEIF(B:B, "<>undefined"), 2), B2), B2)`
Datele sunt disponibile in sheet-ul "curatedData".
Analiza este disponibila in sheet-ul "dataAnalysis".

### Pasul 6: Analiza datelor

In sheet-ul "dataAnalysis2" sunt pastrate datele pentru seria:
SENSE1	SENSE10	SENSE11	SENSE12	SENSE14	SENSE15	SENSE3	SENSE4	SENSE5	SENSE6	SENSE7	SENSE9	dusttrak
fiind eliminate datele care contineau foarte multe valori "undefined" sau valori ~0.

Peste aceste date s-a aplicat procedura descrisa in Pasul 5 pentru a elimina valorile "undefined" folosind formula:
`=IFERROR(IF(dataAnalysis2!B2="undefined", ROUND(AVERAGEIF(dataAnalysis2!B:B, "<>undefined"), 2), dataAnalysis2!B2), dataAnalysis2!B2)`

Astfel, datele curate sunt prezentate in sheet-ul "CuratedData2", fiind evidentiate prin culoarea verde (folosind conditional formatting> new rule> "Use a formula to determine which cells to format" 
valorile identice cu cele din sheet-ul "dataAnalysis2". 

`=B2=dataAnalysis2!B2`

Analiza datelor este prezentata in sheet-ul "DataAnalysis3". 


### Fisierul este disponibil la acest  [link](https://docs.google.com/spreadsheets/d/11QZnU6a8IX8DWd0kZWhw_f2y7BmsPvZS/edit?usp=sharing&ouid=110223860124363980416&rtpof=true&sd=true)

Seturile de date "raw" sunt disponibile [aici](https://drive.google.com/file/d/11Q74gUhhwsgAJtcNf6jJG0M_hQPPJDEK/view?usp=sharing)






# PM10 Sensor Analysis

This report presents an analysis of PM10 measurements from different Libelium smart environment pro stations.

## Load and Examine Data

The first step is to load the data and examine its structure. The data is loaded from a CSV file and the 'Time' column is converted to datetime format.

```python
import pandas as pd

data_path = '/mnt/data/SmartSense-data-as-seriestocolumns-2023-08-04 15_16_50.csv'
data = pd.read_csv(data_path, encoding='utf-8-sig', sep=',', skiprows=1)
data['Time'] = pd.to_datetime(data['Time'])
data.head()
```

## Handle Missing Data

Next, we count the number of missing values in each column.

```python
missing_values = data.isnull().sum()
missing_values
```

## Resample Data to Hourly Averages

We resample the data to an hourly frequency, calculating the mean for each hour.

```python
data.set_index('Time', inplace=True)
data_hourly = data.resample('H').mean()
data_hourly.head()
```

## Calculate Basic Statistics for Each Sensor

We calculate the mean, standard deviation, minimum, and maximum for each sensor.

```python
sensor_stats = data_hourly.describe()
sensor_stats
```

## Plot Hourly Averages for Each Sensor

We create a plot to compare the hourly averages recorded by each sensor.

![Hourly Averages for Each Sensor](hourly_averages.png)

## Calculate Pairwise Correlation Coefficients

We calculate correlation coefficients to measure the strength and direction of the relationship between each pair of sensors.

```python
correlations = data_hourly.corr()
correlations
```

And here is the visual representation of the correlation matrix:

![Correlation Matrix](correlation_matrix.png)

#include <WaspFrame.h>
#include <WaspSensorGas_Pro.h>
#include <WaspWIFI_PRO_V3.h>


// define variable
uint8_t socket = SOCKET0;

// choose HTTP server settings
///////////////////////////////////////
char type[] = "http";
char host[] =  "82.78.81.162";  // "10.0.8.130"


uint16_t port =  43080; // 80
///////////////////////////////////////

uint8_t error;
uint8_t status;


float concentration_H2 = -10; // Stores the concentration level in
float concentration_O2 = -10; // Stores the concentration level in

float temperature = -10;      // Stores the temperature in ºC
float humidity = -10;         // Stores the realitve humidity in %RH
float pressure = -10;         // Stores the pressure in Pa


bmeGasesSensor bme;
Gas gas_O2(SOCKET_2); // H2 1st sensor
Gas gas_H2(SOCKET_3); // H2 2nd sensor


void readSensors() {

  USB.println(F("MEASURING SENSORS"));
  delay(100);

  // Reading BME
  bme.ON();

  temperature = bme.getTemperature();
  humidity = bme.getHumidity();
  pressure = bme.getPressure();

  bme.OFF();
  USB.println(F("****************************************"));

  // Read sensors
  //*************************
  //Reading electrochemical sensors
  gas_O2.ON();
  gas_H2.ON();


  //  USB.println(F("... Enter deep sleep mode 3 minutes to warm up sensors"));
  //  PWR.deepSleep("00:00:03:00", RTC_OFFSET, RTC_ALM1_MODE1, SENSOR_ON);

  concentration_O2 = gas_O2.getConc(temperature);
  concentration_H2 = gas_H2.getConc(temperature);

  gas_O2.OFF();
  gas_H2.OFF();


  USB.println(F("****************************************"));

  //  xbee868LP.ON( SOCKET1 );

  ////////////////////////////
  //SHOWING RESULTS OF THE MEASURENMENTS
  ////////////////////////////
  //BME280 measure
  USB.println(F("... MEASUREMENT RESULTS..."));
  USB.println(F("... *************************************"));
  USB.print(F("... Ambient temperature --> "));
  USB.print(temperature);
  USB.println(F(" ºC"));
  USB.print(F("... Ambient Humidity --> "));
  USB.print(humidity);
  USB.println(F(" %"));
  USB.print(F("... Ambient pressure --> "));
  USB.print(pressure);
  USB.println(F(" Pa"));

  //Electrochemical sensor measure
  USB.print(F("... H2 1st sensor concentration: "));
  USB.print(concentration_O2);
  USB.println(F(" ppm"));
  USB.print(F("... H2 2nd sensorconcentration: "));
  USB.print(concentration_H2);
  USB.println(F(" ppm"));
  USB.println(F("... *************************************"));

}

//====================================================================
// Create a Data Frame Lorawan
//====================================================================
void makeFrame(uint8_t frame_type) {

  // 1.1. create new frame
  USB.println(F("..CREATING FRAME PROCESS "));
  frame.createFrame(frame_type);

  // 1.2. add frame fields
  frame.addSensor(SENSOR_STR, "Complete example message");
  frame.addSensor(SENSOR_BAT, PWR.getBatteryLevel() );
  frame.addSensor(SENSOR_GASES_PRO_TC, temperature);
  frame.addSensor(SENSOR_GASES_PRO_HUM, humidity);
  frame.addSensor(SENSOR_GASES_PRO_PRES, pressure);
  frame.addSensor(SENSOR_GASES_PRO_H2, concentration_H2);
  frame.addSensor(SENSOR_GASES_PRO_O2, concentration_O2);

  USB.println(F("\n1. Created frame to be sent"));
  frame.showFrame();


}

//====================================================================
// Send Data Frame
//====================================================================
void sendPacket() {
  // http frame
  error = WIFI_PRO_V3.sendFrameToMeshlium( type, host, port, frame.buffer, frame.length);

  // check response
  if (error == 0)
  {
    USB.println(F("Send frame to meshlium done"));
  }
  else
  {
    USB.println(F("Error sending frame"));
    if (WIFI_PRO_V3._httpResponseStatus)
    {
      USB.print(F("HTTP response status: "));
      USB.println(WIFI_PRO_V3._httpResponseStatus);
    }
    else
    {
      USB.print(F("2. WiFi is connected ERROR"));
    }
  }
}

void setup()
{
  // init USB port
  USB.ON();
  USB.println(F("BEIA Hydrogen sensors"));

  // set Waspmote identifier
  frame.setID("H2");
  USB.println(RTC.getTime());
  USB.println(F("... Enter deep sleep mode 5 minutes to warm up sensors"));
  PWR.deepSleep("00:00:05:00", RTC_OFFSET, RTC_ALM1_MODE1, SENSOR_ON);
  USB.println(RTC.getTime());

}

void loop()
{
  // New iteration
  USB.ON();
  USB.println(F("\n*****************************************************"));
  USB.print(F("New iteration for BEIA "));
  USB.println(F("\n*****************************************************"));

  // Turn on RTC and get starting time
  RTC.ON();

  //////////////////////////////////////////////////
  // 1. Switch ON
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.ON(socket);

  if (error == 0)
  {
    USB.println(F("WiFi switched ON"));
  }
  else
  {
    USB.println(F("WiFi did not initialize correctly"));
  }

  // check connectivity
  status =  WIFI_PRO_V3.isConnected();

  // check if module is connected
  if (status == true)
  {
    // Step 1: Read suitable sensors
    readSensors();

    // Step 2: Create data Frame
    USB.println("Create ASCII Frame");
    makeFrame(BINARY);

    // Step 3: Save on SD card
    //writeSD();

    // Step 4: Send data
    int attempts = 0;
    bool sent_successfully = false;

    while (attempts < 5 && !sent_successfully)
    {
      sendPacket();

      if (error == 0)
      {
        sent_successfully = true;
      }
      else
      {
        attempts++;
        USB.print(F("Error sending frame, retry attempt "));
        USB.println(attempts);
      }
    }
  }

  USB.println(F("---------------------------------"));
  USB.println(F("...Enter deep sleep mode 5 seconds"));
  PWR.deepSleep("00:00:00:05", RTC_OFFSET, RTC_ALM1_MODE1, SENSOR_ON);
  USB.ON();
  USB.print(F("...wake up!! Date: "));
  USB.println(RTC.getTime());

  RTC.setWatchdog(720); // 12h in minutes
  USB.print(F("...Watchdog :"));
  USB.println(RTC.getWatchdog());
  USB.println(F("****************************************"));

}

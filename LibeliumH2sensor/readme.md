# Programming Libelium Waspmote Gas Sensor Pro Unit with Two Hydrogen Sensors

This guide will walk you through the process of programming a Libelium Waspmote Gas Sensor Pro unit with two hydrogen sensors.

## Prerequisites

- Libelium Waspmote Gas Pro BOARD
- WiFi module
- temperature, relative humidity, atmospheric pressure sensor
- 2 Hydrogen sensors [specs](https://development.libelium.com/gases_pro_sensor_guide/sensors#molecular-hydrogen-h2-gas-sensor-calibrated)
- Libelium Waspmote IDE

## Step 1: Set up the Hardware

1. The Waspmote board needs to be connected to a laptop through the USB cable.

## Step 2: Install the Software

1. Install the Waspmote IDE from this link (Windows version) [Link](https://development.libelium.com/waspmote-ide-v06/download-ide-windows)

## Step 3: Configure the WiFi network

1. First, we need to upload this code to configure the WiFi [network](https://gitlab.com/adrianpasat/adi-repo/-/blob/main/LibeliumH2sensor/WiFinetwork.pde)
2. The Waspmote IDE Guide is available [here](https://development.libelium.com/ide-user-guide/environmentota-compatibility)

## Step 4: Read the sensors and send the measurements through WiFi

1. After uploading the WiFi network code we can upload the code to read and send the measurements 
Code link [here](https://gitlab.com/adrianpasat/adi-repo/-/blob/main/LibeliumH2sensor/H2readingWiFi.pde)
2. For data visualization you can see the output on serial monitor and also in this [dashboard](https://grafana.beia-telemetrie.ro/d/REDnMzs4z/hydrogen-sensor-readings?orgId=15&from=1682477672581&to=1682499272581)
3. User : hydrogen / Password : user

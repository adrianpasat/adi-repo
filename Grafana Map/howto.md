# Configurarea Grafana cu Google Sheets și Pluginul Geomap

Această documentație descrie cum să configurați Grafana pentru a afișa date de la stațiile de telemetrie pe un panou Geomap, utilizând Google Sheets ca sursă de date.

## Configurarea Google API

1. Verificați dacă Google Sheets API este activat pentru proiectul dvs. în Google Cloud Console. Pentru a rezolva această problemă, va trebui să activați Google Sheets API pentru proiectul dvs. Iată pașii pentru a face acest lucru:

2. Accesați [Google Cloud Console](https://console.cloud.google.com/).

3. Dacă vi se solicită, conectați-vă cu contul dvs. Google.

4. Ar trebui să vedeți un tablou de bord pentru proiectul dvs. Căutați "Google Sheets API" în lista de API-uri. Dacă nu este acolo, îl puteți căuta folosind bara de căutare din partea de sus a paginii.

5. Faceți clic pe "Google Sheets API" pentru a merge la pagina sa de prezentare generală.

6. Pe pagina Google Sheets API, faceți clic pe butonul "Enable".

7. Așteptați câteva minute pentru ca modificările să se propage prin sistemele Google.

După ce ați făcut acest lucru, încercați din nou să conectați Grafana la sursa dvs. de date Google Sheets. Dacă încă întâmpinați probleme, asigurați-vă că proiectul dvs. Google Cloud este configurat corect cu acreditările API necesare și că aceste acreditări sunt configurate corect în Grafana.

## Configurarea Panoului Geomap în Grafana

1. În Grafana, adăugați sau editați un panou Geomap.
2. În setările panoului, mergeți la fila 'Data'.
3. În fila 'Data', selectați sursa de date Google Sheets.
4. În câmpul 'Range', introduceți intervalul datelor dvs. în Google Sheets. De exemplu, dacă datele dvs. sunt în coloanele A până la D și încep de la rândul 1, ați introduce `Sheet1!A1:C`.
5. Google Spreadsheet folosit [Google Excel](https://docs.google.com/spreadsheets/d/1xOQJ9q60TfrNFFw3BA2-JAfmLpVuD14XIp-xgp8DNqQ/edit?usp=sharing).


## Configurarea Variabilei în Grafana

1. În Grafana, creați o variabilă pentru categoria dvs. În setările variabilei, setați 'Type' la 'Custom' și introduceți numele foilor dvs. în câmpul 'Values', separate prin virgule. De exemplu, ați introduce `Air Quality, Water Quality`.
2. În panoul Geomap, setați 'Data source' la sursa de date Google Sheets și introduceți 'Range' ca `[[Category]]!A1:C`, înlocuind 'Category' cu numele variabilei dvs. Acest lucru va schimba dinamic intervalul în funcție de categoria selectată.
Acum, când selectați o categorie din meniul derulant din partea de sus a tabloului de bord, Grafana va importa datele din foaia corespunzătoare din documentul Google Sheets și le va afișa pe panoul Geomap.


**1.** Check the station - use factory default from [here](https://development.libelium.com/waspmote-factory-default/)

**2.** Check 4G communication - test sending frame to meshlium GW
using the code [here](https://development.libelium.com/4g-08a-send-to-meshlium-http/)

**3.** Read sensors for calibration process with these codes here
   - **pH**
   - **ORP**
   - **conductivity**
   - **DO**

**4.** read measurements and send to GW

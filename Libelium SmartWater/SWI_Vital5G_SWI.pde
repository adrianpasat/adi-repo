/*
Smart Water Ions - Libelium Plug and Sense
Sensors:
Ca - socket A
Cl - socket B
pH - socket C
Fl - socket D
Ref - socket E
WT - socket F
*/

//#include <WaspXBee868LP.h>
#include <WaspFrame.h>
#include <WaspWIFI_PRO_V3.h>
#include <smartWaterIons.h>

uint8_t socket = SOCKET0;
// choose HTTP server settings
///////////////////////////////////////
char type[] = "http";
char host[] =  "10.0.8.130";  // 82.78.81.162
uint16_t port =  80; //43080;
///////////////////////////////////////
uint8_t error;
int status;
char SD_FILENAME[] = "IOTDATA.TXT";
bool NTP_IS_SYNC = false;
// define the Waspmote ID
// All Ion sensors can be connected in the four sockets
// In Plug&Sense SOCKETE is reserved for reference probe
//======================================================================
// Plug&Sense SOCKETS
//======================================================================
ionSensorClass calciumSensor(SOCKET_A);
ionSensorClass ClSensor(SOCKET_B);
ionSensorClass pHSensor(SOCKET_C);
ionSensorClass fluorideSensor(SOCKET_D);
pt1000Class tempSensor;
float tempValue;
float pHValue;
float pHVolts;
float CaVolts;
float calciumValue;
float ClVolts;
float ClValue;
float flourVolts;
float flourideValue;
//======================================================================
// Calibration concentrations solutions used in the process
//======================================================================
#define point1 10.0
#define point2 100.0
#define point3 1000.0
//======================================================================
// Calibration voltage values for Calcium sensor
//======================================================================
#define point1_volt_Ca 2.163
#define point2_volt_Ca 2.296
#define point3_volt_Ca 2.425
//======================================================================
// Calibration voltage values for Cl sensor
//======================================================================
#define point1_volt_Cl 3.080
#define point2_volt_Cl 2.900
#define point3_volt_Cl 2.671
//======================================================================
// Calibration voltage values for Fluor sensor
//======================================================================
#define point1_volt_F 3.115
#define point2_volt_F 2.834
#define point3_volt_F 2.557
//======================================================================
// Calibration values for pH sensor
//======================================================================
#define cal_point_10 1.405
#define cal_point_7  2.048
#define cal_point_4 2.687
#define cal_temperature 22.0
//======================================================================
// Define the number of calibration points
//======================================================================
#define NUM_POINTS 3

//======================================================================
const float concentrations[] = { point1, point2, point3 };
const float voltages_Ca[]    = { point1_volt_Ca, point2_volt_Ca, point3_volt_Ca};
const float voltages_Cl[]   = { point1_volt_Cl, point2_volt_Cl, point3_volt_Cl };
const float voltages_F[]     = { point1_volt_F, point2_volt_F, point3_volt_F };
//======================================================================


void readSensors() {
  USB.println(F("MEASURING SENSORS"));
  delay(100);
  SWIonsBoard.ON();
  delay(2000);
  //   2. Read sensors
  /////////////////////////////////////////
  // Read the Calcium sensor
  CaVolts = calciumSensor.read();
  calciumValue = calciumSensor.calculateConcentration(CaVolts);
  delay(500);
  // Read the NO3 sensor
  ClVolts = ClSensor.read();
  ClValue = ClSensor.calculateConcentration(ClVolts);
  delay(500);
  // Read the Fluoride sensor
  flourVolts = fluorideSensor.read();
  flourideValue = fluorideSensor.calculateConcentration(flourVolts);
  delay(500);
  //  Read the Temperature sensor
  tempValue = tempSensor.read();
  delay(500);
  // Read the pH sensor
  pHVolts = pHSensor.read();
  pHValue = pHSensor.pHConversion(pHVolts, tempValue);
  delay(500);
  ///////////////////////////////////////////
  // 3. Turn off the sensors
  ///////////////////////////////////////////
  SWIonsBoard.OFF();
}


void makeFrame(uint8_t frame_type) {
  // 1.1. create new frame
  USB.println(F("..CREATING FRAME PROCESS "));
  frame.createFrame(frame_type);
  // 1.2. add frame fields
  frame.addSensor(SENSOR_STR, "Measurements");
  frame.addSensor(SENSOR_BAT, PWR.getBatteryLevel() );
  frame.addSensor(SENSOR_IONS_WT, tempValue);
  frame.addSensor(SENSOR_IONS_PH, pHValue);
  frame.addSensor(SENSOR_IONS_CA, calciumValue);
  frame.addSensor(SENSOR_IONS_CL, ClValue);
  frame.addSensor(SENSOR_IONS_FL, flourideValue);
  USB.println(F("\n1. Created frame to be sent"));
  frame.showFrame();
}

//====================================================================
// Send Data Frame
//====================================================================
void sendPacket() {
  // http frame
  error = WIFI_PRO_V3.sendFrameToMeshlium( type, host, port, frame.buffer, frame.length);

  // check response
  if (error == 0)
  {
    USB.println(F("Send frame to meshlium done"));
  }
  else
  {
    USB.println(F("Error sending frame"));
    if (WIFI_PRO_V3._httpResponseStatus)
    {
      USB.print(F("HTTP response status: "));
      USB.println(WIFI_PRO_V3._httpResponseStatus);
    }
    else
    {
      USB.print(F("2. WiFi is connected ERROR"));
    }
  }
}


bool writeSD(void) {
  bool ok = true;
  USB.println(F("--------------- Start of writeSD ------------------------"));
  uint8_t sd_status = 0;
  char epoch_time_str[16];
  char millis_str[16];

  // Start SD
  SD.ON();

  // Open file
  SdFile file;
  sd_status = SD.openFile(SD_FILENAME, &file, O_APPEND | O_CREAT | O_RDWR);
  if (sd_status == 1) {
    USB.println(F("Succesfully oppened file"));
  } else {
    USB.println(F("Failed to open file."));
    return false;
  }

  // Add newline
  ok &= file.write("\n") > 0;

  if (NTP_IS_SYNC == true) {
    USB.println(F("NTP is set - I will write the real time on the SD card."));
    ok &= file.write("+\t") > 0;
  } else {
    USB.println(F("No NTP was synced. No time available :("));
    ok &= file.write("-\t") > 0;
  }

  ltoa(millis(), millis_str, 10);
  ok &= file.write(millis_str) > 0;
  ok &= file.write("\t") > 0;

  // Write epoch time (or *)
  if (NTP_IS_SYNC) {
    ltoa(RTC.getEpochTime(), epoch_time_str, 10);
    ok &= file.write(epoch_time_str) > 0;
  } else {
    ok &= file.write("*") > 0;
  }

  // Write \t before frame
  ok &= file.write("\t") > 0;

  ok &= file.write(frame.buffer, frame.length) > 0;

  if (ok) {
    USB.println(F("All write operation were succesful"));
  } else {
    USB.println(F("Some errors when writting."));
  }
  // Close the file
  SD.closeFile(&file);

  // Stop SD
  SD.OFF();
  USB.println(F("--------------- End of writeSD ------------------------"));
  return ok;
}


void setup()
{
  // Turn ON the Smart Water Ions Board and USB
  SWIonsBoard.ON();
  USB.ON();
  USB.println(F("Water Quality SWI"));

  // set Waspmote identifier
  frame.setID("Vital5G_Apa");
  // Calibrate the Calcium sensor
  calciumSensor.setCalibrationPoints(voltages_Ca, concentrations, NUM_POINTS);
  // Calibrate the NO3 sensor
  ClSensor.setCalibrationPoints(voltages_Cl, concentrations, NUM_POINTS);
  // Calibrate the Fluoride sensor
  fluorideSensor.setCalibrationPoints(voltages_F, concentrations, NUM_POINTS);
  // Calibrate the pH sensor
  pHSensor.setpHCalibrationPoints(cal_point_10, cal_point_7, cal_point_4, cal_temperature);

}

void loop()
{
  // New iteration
  USB.ON();
  USB.println(F("\n*****************************************************"));
  USB.print(F("New iteration for BEIA "));
  USB.println(F("\n*****************************************************"));

  // Turn on RTC and get starting time
  RTC.ON();
  USB.println(RTC.getTime());
  // Step 1: Read suitable sensors
  readSensors();

  USB.println(RTC.getTime());
  // Step 2: Create data Frame
  USB.println("Create ASCII Frame");
  makeFrame(ASCII);
  writeSD();
  USB.println("Wrote on SD Card ");
  makeFrame(BINARY);
  //////////////////////////////////////////////////
  // 1. Switch ON
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.ON(socket);

  if (error == 0)
  {
    USB.println(F("WiFi switched ON"));
  }
  else
  {
    USB.println(F("WiFi did not initialize correctly"));
  }

  // check connectivity
  status =  WIFI_PRO_V3.isConnected();

  // check if module is connected
  if (status == true)
  {

    // Step 4: Send data
    int attempts = 0;
    bool sent_successfully = false;

    while (attempts < 10 && !sent_successfully)
    {
      sendPacket();

      if (error == 0)
      {
        sent_successfully = true;
      }
      else
      {
        attempts++;
        USB.print(F("Error sending frame, retry attempt "));
        USB.println(attempts);
      }
    }
  }

  USB.println(F("---------------------------------"));
  USB.println(F("...Enter deep sleep mode 16 min"));
  PWR.deepSleep("00:00:00:10", RTC_OFFSET, RTC_ALM1_MODE1, ALL_OFF);
  USB.ON();
  USB.print(F("...wake up!! Date: "));
  USB.println(RTC.getTime());

  RTC.setWatchdog(720); // 12h in minutes
  USB.print(F("...Watchdog :"));
  USB.println(RTC.getWatchdog());
  USB.println(F("****************************************"));
}


/*
                    | A | B | C | D | E | F |
                    |-----------------------|
  BME280            |   |   |   |   | X |   |
  CO2               | X |   |   |   |   |   |
  NO2               |   | X |   |   |   |   |
  O3                |   |   | X |   |   |   |
  PM                |   |   |   | X |   |   |
  SO2               |   |   |   |   |   | X |


  Communication:
  4G

  Extra functions:

*/
#include <WaspSensorSW.h>
#include <WaspFrame.h>
#include <Wasp4G.h>

// Default device name
char *MOTE_ID = "SW1";

//====================================================================
// PARAMETERS FOR SD CARD
//===================================================================

char SD_FILENAME[] = "IOTDATA.TXT";

//====================================================================
// PARAMETERS TO CONFIGURE 4G RADIO
//===================================================================
char apn[] = "net";
char login[] = "";
char password[] = "";
char PIN[] = "";

// SERVER settings
///////////////////////////////////////
char *hosts[] = {"82.78.81.172", "82.78.81.178"};
uint16_t ports [] = {6069, 80};
int n_hosts = 2;
//#############################################
// battery control variables
uint8_t battery;
// other variables
uint8_t error;
//uint8_t error_flag;
//int8_t cont;
uint8_t connection_status;
//############################################
float value_pH;
float value_temp;
float value_pH_calculated;
float value_orp;
float value_orp_calculated;
float value_di;
float value_do;
float value_do_calculated;
float value_cond;
float value_cond_calculated;

// Calibration values
#define cal_point_10 1.982
#define cal_point_7 2.088
#define cal_point_4 2.258
// Temperature at which calibration was carried out
#define cal_temp 24.2
// Offset obtained from sensor calibration
#define calibration_offset 0.0065
// Calibration of the sensor in normal air
#define air_calibration 0.414
// Calibration of the sensor under 0% solution
#define zero_calibration 0.000
// Value 1 used to calibrate the sensor
#define point1_cond 1413
// Value 2 used to calibrate the sensor
#define point2_cond 12880
// Point 1 of the calibration
#define point1_cal 1173.55
// Point 2 of the calibration
#define point2_cal 118.75
//#############################################
pHClass pHSensor;
ORPClass ORPSensor;
DIClass DISensor;
DOClass DOSensor;
conductivityClass ConductivitySensor;
pt1000Class TemperatureSensor;

/////////////////////////////////////////////////
// Global flags (do not change until you know what are you doing
////////////////////////////////////////////////
bool NTP_IS_SYNC = false;
bool LW_IS_SET = false;

void readSensors() {
  delay(100);
  ///////////////////////////////////////////
  // 1. Turn on the board
  ///////////////////////////////////////////
  Water.ON();
  delay(2000);
  ///////////////////////////////////////////
  // 2. Read sensors
  ///////////////////////////////////////////

  // Read the ph sensor
  value_pH = pHSensor.readpH();
  // Read the temperature sensor
  value_temp = TemperatureSensor.readTemperature();
  // Convert the value read with the information obtained in calibration
  value_pH_calculated = pHSensor.pHConversion(value_pH, value_temp);
  // Reading of the ORP sensor
  value_orp = ORPSensor.readORP();
  // Apply the calibration offset
  value_orp_calculated = value_orp - calibration_offset;
  // Reading of the DI sensor
  value_di = DISensor.readDI();
  // Reading of the ORP sensor
  value_do = DOSensor.readDO();
  // Conversion from volts into dissolved oxygen percentage
  value_do_calculated = DOSensor.DOConversion(value_do);
  // Reading of the Conductivity sensor
  value_cond = ConductivitySensor.readConductivity();
  // Conversion from resistance into ms/cm
  value_cond_calculated = ConductivitySensor.conductivityConversion(value_cond);

  ///////////////////////////////////////////
  // 3. Turn off the sensors
  ///////////////////////////////////////////

  Water.OFF();
}
//##############  WRITE SD  ##################################
bool writeSD(void) {
  bool ok = true;
  USB.println(F("--------------- Start of writeSD ------------------------"));
  uint8_t sd_status = 0;
  char epoch_time_str[16];
  char millis_str[16];

  // Start SD
  SD.ON();

  // Open file
  SdFile file;
  sd_status = SD.openFile(SD_FILENAME, &file, O_APPEND | O_CREAT | O_RDWR);
  if (sd_status == 1) {
    USB.println(F("Succesfully oppened file"));
  } else {
    USB.println(F("Failed to open file."));
    return false;
  }

  // Add newline
  ok &= file.write("\n") > 0;

  if (NTP_IS_SYNC == true) {
    USB.println(F("NTP is set - I will write the real time on the SD card."));
    ok &= file.write("+\t") > 0;
  } else {
    USB.println(F("No NTP was synced. No time available :("));
    ok &= file.write("-\t") > 0;
  }

  ltoa(millis(), millis_str, 10);
  ok &= file.write(millis_str) > 0;
  ok &= file.write("\t") > 0;

  // Write epoch time (or *)
  if (NTP_IS_SYNC) {
    ltoa(RTC.getEpochTime(), epoch_time_str, 10);
    ok &= file.write(epoch_time_str) > 0;
  } else {
    ok &= file.write("*") > 0;
  }

  // Write \t before frame
  ok &= file.write("\t") > 0;

  ok &= file.write(frame.buffer, frame.length) > 0;

  if (ok) {
    USB.println(F("All write operation were succesful"));
  } else {
    USB.println(F("Some errors when writting."));
  }
  // Close the file
  SD.closeFile(&file);

  // Stop SD
  SD.OFF();
  USB.println(F("--------------- End of writeSD ------------------------"));
  return ok;
}


//====================================================================
// Create a Data Frame Lorawan
//====================================================================
void CreateDataFrame(uint8_t frame_type) {
  USB.println(F("..CREATING FRAME PROCESS "));
  frame.createFrame(frame_type, MOTE_ID);
  frame.addTimestamp();
  // set frame fields (Sensors Values)
  frame.addSensor(SENSOR_BAT, battery);
//  / Add temperature
  frame.addSensor(SENSOR_WATER_WT, value_temp);
  // Add PH
  frame.addSensor(SENSOR_WATER_PH, value_pH_calculated);
  // Add ORP value
  frame.addSensor(SENSOR_WATER_ORP, value_orp_calculated);
  // Add DO value
  frame.addSensor(SENSOR_WATER_DO, value_do_calculated);
  // Add conductivity value
  frame.addSensor(SENSOR_WATER_COND, value_cond_calculated);

  frame.showFrame();
}

//====================================================================
// Configure 4G module
//====================================================================
void set4G() {
  //////////////////////////////////////////////////
  // 1. sets operator parameters
  //////////////////////////////////////////////////
  USB.println("SETTING 4G PARAMETERS...");
  _4G.set_APN(apn, login, password);

  //////////////////////////////////////////////////
  // 2. Show APN settings via USB port
  //////////////////////////////////////////////////
  _4G.show_APN();

  //////////////////////////////////////////////////
  // 4. set PIN
  //////////////////////////////////////////////////
  if (!strcmp(PIN, ""))
    return;

  USB.println(F("Setting PIN code..."));
  if (_4G.enterPIN(PIN) == 0) {
    USB.println(F("PIN code accepted"));
  } else {
    USB.println(F("PIN code incorrect"));
  }
}

//====================================================================
// Set time from 4G
//====================================================================
void setTime4G() {
  USB.println(F("Setting time from 4G...."));
  error = _4G.ON();

  if (error == 0) {
    USB.println(F("4G module ready..."));

    ////////////////////////////////////////////////
    // Check connection to network and continue
    ////////////////////////////////////////////////
    connection_status = _4G.checkDataConnection(30);
    delay(1000);
    //////////////////////////////////////////////////
    // 3. set time
    //////////////////////////////////////////////////
    if (connection_status == 0) {
      if (_4G.setTimeFrom4G() == 0) {
        USB.println(F("Succesufully set time from 4G"));
        NTP_IS_SYNC = true;
      } else {
        USB.println(F("Failed to get time from 4G"));
      }
    }
  } else {
    // Problem with the communication with the 4G module
    USB.println(F("4G module not started"));
    USB.print(F("Error code: "));
    USB.println(error, DEC);
  }
  ////////////////////////////////////////////////
  // 4. Powers off the 4G module
  ////////////////////////////////////////////////
  USB.println(F("Switch OFF 4G module\n"));
  _4G.OFF();
}

//====================================================================
// Send Data Frame 4G
//====================================================================
void send4G(char *host, uint16_t port) {
  const int maxRetries = 5;
  int retryCount = 0;

  while (retryCount < maxRetries) {
    error = _4G.ON();

    if (error == 0) {
      USB.print(F("4G module ready... Sending to "));
      USB.println(host);

      ////////////////////////////////////////////////
      // Check connection to network and continue
      ////////////////////////////////////////////////
      connection_status = _4G.checkDataConnection(30);
      delay(500);

      ////////////////////////////////////////////////
      // 3. Send to Meshlium
      ////////////////////////////////////////////////
      USB.print(F("Sending the frame..."));
      error = _4G.sendFrameToMeshlium(host, port, frame.buffer, frame.length);

      // check the answer
      if (error == 0) {
        USB.print(F("Done. HTTP code: "));
        USB.println(_4G._httpCode);
        USB.print("Server response: ");
        USB.println(_4G._buffer, _4G._length);
        break; // Exit the loop if data sent successfully
      } else {
        USB.print(F("Failed. Error code: "));
        USB.println(error, DEC);
        retryCount++;
      }
    } else {
      // Problem with the communication with the 4G module
      USB.println(F("4G module not started"));
      USB.print(F("Error code: "));
      USB.println(error, DEC);
      retryCount++;
    }

    // Retry delay
    if (retryCount < maxRetries) {
      delay(500); // You can adjust the delay between retries if needed
    }
  }

  ////////////////////////////////////////////////
  // 4. Powers off the 4G module
  ////////////////////////////////////////////////
  USB.println(F("Switch OFF 4G module\n"));
  _4G.OFF();
}

//############################################################################################################


void setup() {
  uint8_t error;
  // Turn ON the USB and print a start message
  USB.ON();
  delay(100);
  USB.println(F("\n*****************************************************"));
  USB.print(F("BEIA "));
  USB.println(MOTE_ID);
  USB.println(F("*****************************************************"));
  // Configure the calibration values
  pHSensor.setCalibrationPoints(cal_point_10, cal_point_7, cal_point_4, cal_temp);
  DOSensor.setCalibrationPoints(air_calibration, zero_calibration);
  ConductivitySensor.setCalibrationPoints(point1_cond, point1_cal, point2_cond, point2_cal);
  // Init RTC
  RTC.ON();
  // Set 4G
  set4G();
  // Set time from 4G
  setTime4G();
  // Getting time
  USB.print(F("Time [Day of week, YY/MM/DD, hh:mm:ss]: "));
  USB.println(RTC.getTime());
}

//############################################################################################################

void loop() {
  // New iteration
  USB.ON();
  USB.println(F("\n*****************************************************"));
  USB.print(F("New iteration for BEIA "));
  USB.println(MOTE_ID);
  USB.println(F("*****************************************************"));

  // Turn on RTC and get starting time
  RTC.ON();

  // Check battery level
  battery = PWR.getBatteryLevel();
  USB.print(F("Battery level: "));
  USB.println(battery, DEC);

  // Step 1: Read suitable sensors
  readSensors();
  // Step 2: Create data Frame
  USB.println("Create ASCII Frame");
  CreateDataFrame(ASCII);
  // Step 3: Save on SD card
  writeSD();
  // Step 4: If enough battery, send data
  if (battery >= 20) {
    if (!NTP_IS_SYNC) {
      USB.println("Time not set... retry...");
      setTime4G();
    }
   
    // Step 4.2: Send using 4G
    for(int i = 0; i < n_hosts; i++) {
      send4G(hosts[i], ports[i]);
    }
    
  } else {
    USB.println(F("Skip seding data... battery under 30%"));
  }

  USB.println(F("---------------------------------"));
  USB.println(F("...Enter deep sleep mode 15 min"));
  PWR.deepSleep("00:00:05:05", RTC_OFFSET, RTC_ALM1_MODE1, ALL_OFF);
  USB.ON();
  USB.print(F("...wake up!! Date: "));
  USB.println(RTC.getTime());

  RTC.setWatchdog(720); // 12h in minutes
  USB.print(F("...Watchdog :"));
  USB.println(RTC.getWatchdog());
  USB.println(F("****************************************"));
}

//***********************************************************************************************
// END OF THE SKETCH
//************************


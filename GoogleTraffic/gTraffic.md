
# Google Maps Traffic Layer Experiment in Google Cloud Shell

This guide provides the exact steps to clone, set up, and run the Google Maps Traffic Layer sample in Google Cloud Shell.

## Prerequisites

- Google Cloud Platform account
- Google Cloud Shell access

## Instructions

1. **Open Google Cloud Shell**: Navigate to the [Google Cloud Platform Console](https://console.cloud.google.com/) and click the Cloud Shell icon.

2. **Clone the Repository and Set Up the Project**: Run the following command to clone the specific branch and set up the environment.

   ```bash
   cloudshell_open --repo_url "https://github.com/googlemaps/js-samples" --page "editor" --tutorial "cloud_shell_instructions.md" --open_workspace "." --git_branch "sample-layer-traffic" --force_new_clone
   ```

3. **Install Dependencies**: Navigate to the project directory and install the required dependencies using npm.

   ```bash
   cd ~/cloudshell_open/js-samples-0
   npm i
   ```

4. **Edit Coordinates (Optional)**: If needed, edit the source files (e.g., `src/index.ts`) to update the coordinates to Bucharest's city center or make other changes.

5. **Start the Development Server**: Start the development server on port 8080.

   ```bash
   npm start -- --port=8080
   ```

6. **Access the Application**: Click the "Web Preview" icon in the top-right corner of the Cloud Shell, then click "Preview on port 8080." The application will be displayed in a new tab.

![Demo Image](https://gitlab.com/adrianpasat/adi-repo/-/raw/3c4974c49e8dbc72694a421c5f225c943e67d95f/GoogleTraffic/demo.png "Demo of Google Traffic")


## Support

For further assistance, please refer to the [official repository](https://github.com/googlemaps/js-samples) or contact your support team.
